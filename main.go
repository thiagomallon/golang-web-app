/**
 * @author Thiago Mallon <thiagomallon@gmail.com>
 */
package main

import (
	"Golang-Web-App/routers"
	"net/http"
)

/**
 *
 */
func main() {
	router := routers.GetRoutes()
	router.ServeFiles("/static/*filepath", http.Dir("public/"))
	panic(http.ListenAndServe("localhost:3000", router))
}
