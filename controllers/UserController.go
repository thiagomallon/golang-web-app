/**
 * @author Thiago Mallon <thiagomallon@gmail.com
 */
package controllers

import (
	"Golang-Web-App/dao"
	"Golang-Web-App/models"
	"html/template"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

/**
 *
 */
type userController struct{}

/**
 *
 */
func UserController() *userController {
	// return &userController{}
	return new(userController)
}

/**
 *
 */
func (uc userController) GetUsers(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	userDAO := dao.UserDAO() // instantiate UserDAO
	users := userDAO.GetUsers()

	type pageData struct {
		Person []models.User
	}

	data := pageData{Person: users}
	// data := map[string][]models.User{"Person": users}
	// data := struct{ Person []models.User }{Person: users}
	t, _ := template.ParseFiles("views/list.html")
	t.Execute(w, data)
}
