/**
 * @author Thiago Mallon <thiagomallon@gmail.com
 */
package models

import "gopkg.in/mgo.v2/bson"

type User struct {
	Id         bson.ObjectId `json:"id" bson:"_id"`
	Name       string        `json:"name" bson:"name"`
	Email      string        `json:"email" bson:"email"`
	Occupation string        `json:"occupation" bson:"occupation"`
	Gender     string        `json:"gender" bson:"gender"`
	Age        int           `json:"age" bson:"age"`
}
