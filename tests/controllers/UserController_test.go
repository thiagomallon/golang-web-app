/**
 * @author Thiago Mallon <thiagomallon@gmail.com
 */
package controllers_test

import (
	. "Golang-Web-App/controllers"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/mgo.v2/bson"
)

var _ = Describe("UserController", func() {
	var (
		userController controllers.UserController()
	)

	BeforeEach(func() {
		userController = User{
			Id:         bson.ObjectId(""),
			Name:       "John O'Brien",
			Email:      "johnthegreenobrien@gmail.com",
			Occupation: "Codering Explorer",
			Gender:     "Male",
			Age:        1000,
		}
	})

	Describe("Getting name prop. from userController obj", func() {
		Context("Setted John O'Brien", func() {
			It("should be John O'Brien", func() {
				Expect(userController.GetName()).To(Equal("John O'Brien"))
			})
		})
	})
})
