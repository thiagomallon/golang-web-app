/**
 * @author Thiago Mallon <thiagomallon@gmail.com
 */
package models_test

import (
	. "Golang-Web-App/models"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/mgo.v2/bson"
)

var _ = Describe("User", func() {
	var (
		user User
	)

	BeforeEach(func() {
		user = User{
			Id:         bson.ObjectId(""),
			Name:       "John O'Brien",
			Email:      "johnthegreenobrien@gmail.com",
			Occupation: "Codering Explorer",
			Gender:     "Male",
			Age:        1000,
		}
	})

	Describe("Getting name prop. from user obj", func() {
		Context("Setted John O'Brien", func() {
			It("should be John O'Brien", func() {
				Expect(user.Name).To(Equal("John O'Brien"))
			})
		})
	})
})
