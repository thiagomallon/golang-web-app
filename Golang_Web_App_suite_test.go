/**
 * @author Thiago Mallon <thiagomallon@gmail.com
 */
package main_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestGolangWebApp(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GolangWebApp Suite")
}
