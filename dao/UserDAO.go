/**
 * @author Thiago Mallon <thiagomallon@gmail.com
 * @created
 */

package dao

import (
	"Golang-Web-App/common/database"
	"Golang-Web-App/models"
	"log"
)

/**
 *
 */
type userDAO struct {
	coll string
}

/**
 *
 */
func UserDAO() *userDAO {
	return &userDAO{"users"}
}

/**
 *
 */
func (us userDAO) GetUsers() []models.User {
	db := database.ConnMongo()
	result := []models.User{}
	err := db.GetCollection("users").Find(nil).Sort("name").All(&result)
	if err != nil {
		log.Fatal(err)
	}
	db.Close()
	return result
}

/**
 *
 */
func (us userDAO) GetUser() models.User {
	db := database.ConnMongo()
	result := models.User{}
	err := db.GetCollection("users").Find(nil).Sort("name").One(&result)
	if err != nil {
		log.Fatal(err)
	}
	db.Close()
	return result
}
