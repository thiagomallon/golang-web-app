/**
 * @author Thiago Mallon <thiagomallon@gmail.com
 */
package database

import (
	"log"

	// "reflect"

	"gopkg.in/mgo.v2"
	// "gopkg.in/mgo.v2/bson"
)

/**
 *
 */
type mongoSession struct {
	session    *mgo.Session
	database   *mgo.Database
	collection *mgo.Collection
}

/**
 * @method mgoSessione.Clone() GetmongoSession It create and instantiate a Mongodb connection
 * @return mgoSessione.Clone()
 */
func ConnMongo() *mongoSession {
	db := &mongoSession{}
	return db.connectFactory()
}

// Singleton variable
var mgoSession *mgo.Session

/**
 * @method connect
 */
func (db *mongoSession) connectSingleton() *mongoSession {
	var HOST string = "localhost"
	var PORT string = "27017"
	var DATABASe string = "go_web_mastering"

	if mgoSession == nil {
		var err error
		mgoSession, err = mgo.Dial("mongodb://" + HOST + ":" + PORT)
		if err != nil {
			log.Fatal("Failed to start the Mongo session")
		}
	}
	db.session = mgoSession.Clone()
	db.UseDB(DATABASe) // it must to be charged be the app context, that is charged by a config file
	return db
}

func (db *mongoSession) connectFactory() *mongoSession {
	var HOST string = "localhost"
	var PORT string = "27017"
	var DATABASe string = "go_web_mastering"

	var err error
	var mgoSession *mgo.Session
	mgoSession, err = mgo.Dial("mongodb://" + HOST + ":" + PORT)
	if err != nil {
		log.Fatal("Failed to start the Mongo session")
	}
	db.session = mgoSession.Clone()
	db.UseDB(DATABASe) // it must to be charged be the app context, that is charged by a config file
	return db
}

/**
 * @method db.db UseDB
 * @param string db
 * @return *mgo.Database db.db
 */
func (db *mongoSession) UseDB(dbase string) *mongoSession {
	db.database = db.session.DB(dbase)
	return db
}

/**
 * @method db.collection GetCollection
 * @param string coll
 * @return *mgo.Collection db.collection
 */
func (db *mongoSession) GetCollection(coll string) *mgo.Collection {
	db.collection = db.database.C(coll)
	return db.collection
}

/**
 * @method void Close
 */
func (db *mongoSession) Close() {
	defer db.session.Close()
}
