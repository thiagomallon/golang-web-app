/**
 * @author Thiago Mallon <thiagomallon@gmail.com
 */
package routers

import (
	"Golang-Web-App/controllers"

	"github.com/julienschmidt/httprouter"
)

/**
 *
 */
func GetRoutes() *httprouter.Router {
	router := httprouter.New()

	// ROUTES
	router.GET("/users", controllers.UserController().GetUsers)

	return router
}
